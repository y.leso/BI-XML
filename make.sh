#!/usr/bin/env bash
####################################################################
#   Author:     Leso Yurii <lesoyuri@fit.cvut.cz> (c) 2020
#   Version:    1.0
####################################################################

LOG='yes'

JING_PATH="./external/jing/bin/jing.jar"
SAXON_PATH="./external/jing/bin/saxon.jar"

DIRECTORY='./src'
OUTPUT_DIR='./build' # './output'

XML_GENERATED_DIR="${OUTPUT_DIR}/xml"
XML_GENERATED_NAME="all-merged.xml"
XML_MERGED_FILE="$XML_GENERATED_DIR/$XML_GENERATED_NAME"

VALIDATORS_DIR="${DIRECTORY}/validation"
DTR_VALIDATOR_FILE="${VALIDATORS_DIR}/test.dtd"
RNG_VALIDATOR_FILE="${VALIDATORS_DIR}/test.rng"
RNC_VALIDATOR_FILE="${VALIDATORS_DIR}/test.rnc"

XSLT_DIR="${DIRECTORY}/xslt"
XSLT_HTML="${XSLT_DIR}/HTML.xslt"
XSLT_PDF="${XSLT_DIR}/PDF.xslt"

CSS_DIR="${DIRECTORY}/css"

OUTPUT_HTML_DIR="${OUTPUT_DIR}/html"
OUTPUT_IMAGES_DIR="${OUTPUT_DIR}/html/images"
OUTPUT_PDF_DIR="${OUTPUT_DIR}/pdf"

####################################################################
#                          Functions
####################################################################
logMSG () { [[ -z LOG ]] || echo -e $@; }
printERR () { >&2 echo "👎 ERROR: $@"; }
separator () { echo "==================================";}

jing () {  java -jar  ${JING_PATH} $@; }
saxon () {  java -jar  ${SAXON_PATH} $@; }

mergeXML () {
    separator;
    ([[ -z "$1" ]] || [[ -z "$2" ]]) && {
        printERR "Usage: mergeXML <FILE> OUTPUT>
    FILE    - path to source file, XML should be merged from
    OUTPUT  - path to file should be created with merged XMLs"
        return 1
    }
    file="$1"
    output="$2"
    output_dir=$(dirname "$output")
    [[ -f "$file" ]] || {
        printERR "Source file (${file})does not exist. What should I merge from?";
        return 1
    }
    logMSG "1. Generate xml file with all countries"
    [[ -d "$output_dir" ]] || {
        logMSG "    1.1 output_dir doesn't exist. Creating";
        mkdir -p "$output_dir"
    }
    xmllint --noent "$file" > "$output"
    return $?
}

validateXML () {
    separator;
    logMSG "2. Validating XML was started"
    [[ -z "$1" ]] && {
        printERR "Usage: validateXML <FILE>
    FILE    - path to xml source file that should be validated"
        return 1
    }
    source="$1";
    [[ -f "$source" ]] || {
        printERR "File to validate doesn't exist. \nNothing to validate"
        return 2
    }
    # -----------------------------------------------------------------
    logMSG "DTD: validation $source"
    xmllint --noout --dtdvalid ${DTR_VALIDATOR_FILE} "$source" || { return $?; }
    logMSG "👍 DTD validation was successfully finished"
    # -----------------------------------------------------------------
    logMSG "RNG: validation $source"
    jing ${RNG_VALIDATOR_FILE} ${source} || { return $?; }
    logMSG "👍 RNG validation was successfully finished"
    # -----------------------------------------------------------------
    logMSG "RNC: validation $source"
    jing -c ${RNC_VALIDATOR_FILE} ${source} || { return $?; }
    logMSG "👍 RNC validation was successfully finished"
    # -----------------------------------------------------------------
    return $?
}

countryName () {
    country=""
    case $1 in
    "EZ")
        echo "Czechia"
        ;;
    "UP")
        echo "Ukraine"
        ;;
    "LO")
        echo "Slovakia"
        ;;
    "GM")
        echo "Germany"
        ;;
    *)
        printERR "Country wasn't detected!"
        throw "Here is no country at list"
        return 1
        ;;
    esac
    return 0
}

convertToHTML () {
    separator;
    logMSG "3. Generating HTML pages from .xml sources"
    ([[ -z "$1" ]] || [[ -z "$2" ]]) && {
        printERR "Usage: convertToHTML <XSLT_TEMPLATE> <XML_DIR> <OUTPUT_DIR>
    XSLT_TEMPLATE - path to xslt template file which will be used to converting
    XML_DIR       - path to directory with XML files should be converted to HTML.
    OUTPUT_DIR    - path to directory where should be stored generated HTML pages
"
        return 1
    }
    xslt_template=${1}
    xml_dir=${2};
    [[ -z ${3} ]] && output_dir="." || output_dir=${3}
    [[ -f ${xslt_template} ]] || {
        printERR "${xslt_template} doesn't exist. How can I convert"
        return 2
    }
    [[ -d ${xml_dir} ]] || {
        printERR "${xml_dir} does not exists. What should I convert?"
        return 3
    }
    [[ -d ${output_dir} ]] || {
        logMSG "    ${output_dir} does not exists. Creating directory"
        mkdir ${output_dir}
    }

    for xml_source in ${xml_dir}/*.xml
    do
        source_name=$(basename ${xml_source} .xml)
        #saxon -s:${xml_source} -xsl:${xslt_template} -o:"${output_dir}/${source_name}" || { return 1; }
        saxon -o "${output_dir}/${source_name}.html" \
              ${xml_source} \
              ${xslt_template} || { return 1; }
        logMSG "👍 ${source_name} was generated"
    done
}

downloadImages (){
    separator
    logMSG "4. Downloading images"
    pathToImage="https://www.cia.gov/library/publications/the-world-factbook/attachments"
    [[ -d ${OUTPUT_IMAGES_DIR} ]] || {
        logMSG "${OUTPUT_IMAGES_DIR} does not exist. Creating..."
        mkdir -p ${OUTPUT_IMAGES_DIR}
    }

    for countyCode in "EZ" "UP" "LO" "GM"
    do
        logMSG "${countyCode}:"
        countryFullName=$(countryName ${countyCode})
        wget "${pathToImage}/flags/${countyCode}-flag.gif" \
             -O "${OUTPUT_IMAGES_DIR}/${countryFullName}-flag.gif" -q || { return 1; }
        logMSG "\t👍 flag is downloaded"
        wget "${pathToImage}/maps/${countyCode}-map.gif" \
             -O "${OUTPUT_IMAGES_DIR}/${countryFullName}-map.gif" -q || { return 1; }
        logMSG "\t👍 map is downloaded"
    done

    logMSG ""
}

copyHTMLFiles () {
    separator;
    logMSG "5. Copying all HTML files is used"
    cp -r "${CSS_DIR}/" "${OUTPUT_HTML_DIR}/css" || { return 1; }
    return $?
}

convertToPDF () {
    separator;
    logMSG "6. Generating PDF pages from .xml sources"
    ([[ -z "$1" ]] || [[ -z "$2" ]]) && {
        printERR "Usage: convertToPDF <XSLT_TEMPLATE> <XML_FILE> <OUTPUT_FILE>
    XSLT_TEMPLATE - path to xslt template file which will be used to converting
    XML_FILE      - path to xml file which should be converted to HTML.
    OUTPUT_FILE   - path to file where should be stored generated HTML pages
"
        return 1
    }
    xslt_template=${1}
    xml_file=${2};
    [[ -z ${3} ]] && output_file="./main.pdf" || output_file=${3}

     [[ -f ${xslt_template} ]] || {
        printERR "${xslt_template} doesn't exist. How can I convert"
        return 2
    }
    [[ -f ${xml_file} ]] || {
        printERR "${xml_file} does not exists. What should I convert?"
        return 3
    }
    [[ -d `dirname ${output_file}` ]] || {
        logMSG "    ${output_file} does not exists. Creating directory"
        mkdir -p `dirname ${output_file}`
    }

     #saxon ./output/xml/all-merged.xml ${XSLT_PDF} > "${OUTPUT_PDF_DIR}/index.pdf"
     saxon ${xml_file} ${xslt_template} > "${output_file}.tmp.pdf" || { return 1; }
     logMSG "👍 Saxon converting was successful"
     (fop -fo "${output_file}.tmp.pdf" -pdf ${output_file} >>/dev/null 2>&1 ) \
     && rm -f "${output_file}.tmp.pdf" || { return 1;}
     logMSG "👍 FOP was successful"
}

####################################################################
#                           Main
####################################################################

main () {
    [[ -d ${OUTPUT_DIR} ]] && {
        echo "I see here is some output directory (${OUTPUT_DIR})"
        echo "Should I remove it"
        echo "[Y]-Yes"
        echo "[N]-No"
        while true;
        do
            read answer
            [[ ${answer} == "Y" ]] && {
                logMSG "Removing output dir."
                rm -rf ${OUTPUT_DIR} \
                && logMSG "👍 Output dir was removed" \
                || { printERR "Removing return error"; return 1; }
                break;
            } || {
                [[ ${answer} == "N" ]] && {
                    echo "Got you :)";
                    break;
                } || echo "Unexpected answer. Write only Y or N";
            }
        done
    }
    # -----------------------------------------------------------------
    mergeXML "${DIRECTORY}/xml/all.xml" ${XML_MERGED_FILE} || {
        printERR "XML merging wasn't be successful"
        return 1
    }
    logMSG "👍 Merging all xml files was finished. Everything is ok"
    # -----------------------------------------------------------------
    validateXML ${XML_MERGED_FILE} || {
        printERR "validateXML return error"
        return 2
    }
    logMSG "👍 ${XML_MERGED_FILE} is fully valid"
    # -----------------------------------------------------------------
    convertToHTML ${XSLT_HTML} "${DIRECTORY}/xml" "${OUTPUT_DIR}/html" || {
        printERR "convertToHTML return error"
        return 3
    }
    logMSG "👍 Converting ho HTML successfully finished"
    # -----------------------------------------------------------------
    downloadImages || {
        printERR "downloadImages return error"
        return 4
    }
    logMSG "👍 Images was successfully downloaded"
    # -----------------------------------------------------------------
    copyHTMLFiles ||  {
        printERR "copyHTMLFiles return error"
        return 5
    }
    logMSG "👍  Copying was successfully finished"
    # -----------------------------------------------------------------
    convertToPDF ${XSLT_PDF} ${XML_MERGED_FILE} ${OUTPUT_PDF_DIR}/main.pdf || {
        printERR "convertToPDFL return error"
        return 2
    }
    logMSG "👍  converted to PDF successfully finished"
    # -----------------------------------------------------------------
    echo
    echo "--------------🙂--------------"
    echo "     Your project is ready    "
    echo "--------------🙂--------------"
    open ${OUTPUT_DIR} > /dev/null
}

main