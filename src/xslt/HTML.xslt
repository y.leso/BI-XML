<?xml version="1.0" encoding="utf-16"?>
<xsl:stylesheet version='2.0'
                xmlns:xsl='http://www.w3.org/1999/XSL/Transform'>

    <xsl:output indent="yes" method="html" version="5.0"/>
    <xsl:template match="/">
        <html>
            <head>
                <meta charset="UTF-8"/>
                <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
                <meta name="viewport" content="width=device-width,initial-scale=1"/>
                <title>
                    <xsl:value-of select="country/@name"/>
                </title>
                <link rel="stylesheet" href="./css/styles.css" type="text/css"/>
            </head>
            <body>
                <nav>
                    <ul>
                        <li><a href="cz.html">Czechia</a></li>
                        <li><a href="ua.html">Ukraine</a></li>
                        <li><a href="sk.html">Slovakia</a></li>
                        <li><a href="de.html">Germany</a></li>
                    </ul>
                </nav>
                <main class="content">
                    <xsl:apply-templates select="country"/>
                </main>
            </body>
        </html>
    </xsl:template>
    <xsl:template match="country">
        <h1><xsl:value-of select="@name"/></h1>
        <ul class="navbar">
            <xsl:for-each select="section">
                <li><a><xsl:attribute name="href">
                            <xsl:value-of select="concat('#', @name)"/>
                        </xsl:attribute>
                        <xsl:value-of select="@name"/>
                    </a>
                </li>
            </xsl:for-each>
        </ul>
        <div class="images">
            <img>
                <xsl:attribute name="src">
                    <xsl:value-of select="concat('./images/', @name, '-flag.gif')"/>
                </xsl:attribute>
            </img>
            <img>
                <xsl:attribute name="src">
                    <xsl:value-of select="concat('./images/', @name, '-map.gif')"/>
                </xsl:attribute>
            </img>
        </div>
        <xsl:apply-templates select="section"/>
    </xsl:template>

    <xsl:template match="section">
        <article>
            <h2>
                <xsl:attribute name="id">
                    <xsl:value-of select="@name"/>
                </xsl:attribute>
                <xsl:value-of select="@name"/>
            </h2>
            <section>
                <xsl:apply-templates select="text"/>
            </section>
        </article>
    </xsl:template>

    <xsl:template match="text">
        <h3 class="title">
            <xsl:value-of select="@name"/>
        </h3>
        <xsl:choose>
            <xsl:when test="data/subdata">
                <ul>
                    <xsl:apply-templates select="*/subdata"/>
                </ul>
            </xsl:when>
            <xsl:otherwise>
                <xsl:apply-templates select="data"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    <xsl:template match="data">
        <p>
            <xsl:value-of select="."/>
        </p>
    </xsl:template>
    <xsl:template match="subdata">
        <li><xsl:value-of select="."/></li>
    </xsl:template>

</xsl:stylesheet>
