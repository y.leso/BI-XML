/*
    Author:     Leso Yurii <lesoyuri@fit.cvut.cz> (c) 2020
    Version:    1.0
*/

// expected DOM of wfb-text-box' class
let node = document.getElementsByClassName('wfb-text-box')[0];


const parseCountryToXML = node => {
    // all we need is children array to work with
    node = node.children;

    const countryName = node.geos_title
        .getElementsByClassName("countryName")[0]
        .innerHTML.trim();

    const getSectionTitle = (node) => node.innerText
        .split("::")[0]
        .toLowerCase()
        .trim();

    const getSectionChildren = (node) => node.parentElement.parentElement.parentElement.nextElementSibling.children;

    //The only illegal characters are &, < and >
    const replaceIllegalCharacters = (str) => str
        .replace(/&/g, '&amp;')
        .replace(/</g, '&lt;')
        .replace(/>/g, '&gt;')
        .trim();

    const expandChild = (node) => {
        let res = ``;
        for (let i = 0; i < node.length; i += 2) {
            const title = node[i].innerText.split(":")[0].trim();
            // if we have one child DOM only - it's text
            // else it's list of <subdata> values
            const content = node[i + 1].children.length === 1
                ? replaceIllegalCharacters(node[i + 1].children[0].innerText.replace(/\s+/g, ' '))
                : Array.prototype.slice.call(node[i + 1].children)
                    .map(({innerText}) =>
                        `<subdata>${replaceIllegalCharacters(innerText.replace(/\s+/g, ' '))}</subdata>`)
                    .join("\n");

            res += `<text name="${title}"><data>${content}</data></text>`
        }
        return res;
    };

    const expandSections = Array.prototype.slice.call(document.getElementsByClassName("question eur_med")).map(
        section => `<section name="${getSectionTitle(section)}">
${expandChild(getSectionChildren(section))}
    </section>`);


    return `<?xml version="1.0" encoding="UTF-16"?>
<!--
    Source: ${window.location}
    Parsed: parser.js (BI-XML sem. work)
    Author: Yurii Leso <lesoyuri@fit.cvut.cz>
-->
<country name="${countryName}">
${expandSections.join("\n")}
</country>`
        .replace(/\s+/g, ' ')
        .replace(/<data><\/data>/g, '<data/>')
        .replace(/<subdata><\/subdata>/g, '<subdata/>')
};

parseCountryToXML(node);
