# BI-XML semestral work 
- Author: Yurii Leso 
- Ac. year: 2019/20
## Task choosing
I have chosen the next regions
- [Czechia](https://www.cia.gov/library/publications/the-world-factbook/geos/ez.html)
- [Ukraine](https://www.cia.gov/library/publications/the-world-factbook/geos/up.html)
- [Slovakia](https://www.cia.gov/library/publications/the-world-factbook/geos/lo.html)
- [Germany](https://www.cia.gov/library/publications/the-world-factbook/geos/gm.html)

## Requirements 
To make this project you need to have all scripts make.sh use of. 
- xmllint `using libxml version 20904`
- fop 
- `jing` and `saxon` installation is not necessary. It's stored in ./external folder to avoid 
possible compatibility issues.  You can easy switch to your `jing` or `saxon`. Just change path to them in config

I run it on MacOs Catalina 10.15.2 (19C57)
```bash
uname -a
Darwin lesoyuri_pc.wifi.nat 19.2.0 Darwin Kernel Version 19.2.0: Sat Nov  9 03:47:04 PST 2019; root:xnu-6153.61.1~20/RELEASE_X86_64 x86_64
```
##  Make
0. `cd` to this folder  
1. Make sure you have execute permission to ./make.sh, if not `chmod +x ./make.sh`
2. Edit config variables if you need 
    ```bash
    LOG='yes'
    
    JING_PATH="./external/jing/bin/jing.jar"
    SAXON_PATH="./external/jing/bin/saxon.jar"
    
    DIRECTORY='./src'
    OUTPUT_DIR='./output'
    
    XML_GENERATED_DIR="${OUTPUT_DIR}/xml"
    XML_GENERATED_NAME="all-merged.xml"
    XML_MERGED_FILE="$XML_GENERATED_DIR/$XML_GENERATED_NAME"
    
    VALIDATORS_DIR="${DIRECTORY}/validation"
    DTR_VALIDATOR_FILE="${VALIDATORS_DIR}/test.dtd"
    RNG_VALIDATOR_FILE="${VALIDATORS_DIR}/test.rng"
    RNC_VALIDATOR_FILE="${VALIDATORS_DIR}/test.rnc"
    
    XSLT_DIR="${DIRECTORY}/xslt"
    XSLT_HTML="${XSLT_DIR}/HTML.xslt"
    XSLT_PDF="${XSLT_DIR}/PDF.xslt"
    
    CSS_DIR="${DIRECTORY}/css"
    
    OUTPUT_HTML_DIR="${OUTPUT_DIR}/html"
    OUTPUT_IMAGES_DIR="${OUTPUT_DIR}/html/images"
    OUTPUT_PDF_DIR="${OUTPUT_DIR}/pdf"
    ```
3. Run project `./make.sh`

**./output is an example of running ./make.sh. If you run it now, all sources will be saved in ./build folder according to config**
## Process
1. I have defined the structure of document 
    ```xml
    <country name="<COUNTRY_NAME>">
    <!-->For each section (f.e. introduction, geography...)-->
        <section name="<SECTION_NAME>">
            <!-->For each subsection (f.e. GEOGRAPHY: location, area...)-->
            <text name="<SUBSECTION_NAME>">
                <!-->For each DOM in subsection (f.e. Paragraph or list of )-->
                <data>
                    <!--if Subsection has one only paragraph:-->
                       <!--SUBSECTION_PARAGRAPH-->
                    <!--else -->
                       <!--For each paragraph on subsection_paragraphs-->
                    <subdata>
                        <!--<SUBSECTION_PARAGRAPH[i]>-->
                    </subdata>
                </data>
            </text>
        </section>
    </country>
    ```
2. According to my structure, written parser to get all data is needed in two clicks. 
    - lang: EcmaScript
    - it\'s enough to run it on console (dev. tools)
    - file: ./src/parser.js
3. Parsed and saved all data to xml
    - location: ./src/xml/
    - filename: <COUNTRY-CODE>.xml (f.e. cz.xml for Czech Republic)
4. Prepared validation files
    - location: ./src/validation/
    - filename: test.<TYPE> (f.e. test.rnc for RelaxNG)
5. Created ./make.sh to run merging process and then validation
    - location: ./make.sh 
    - No arguments are required
    - All paths, config variables .etc can be easily edited at the begining of script
    - **BTW:**  this script is written in [Z Shell](https://en.wikipedia.org/wiki/Z_shell) which I am not good in. 
    Last year I worked in [KornShell](https://en.wikipedia.org/wiki/KornShell) only. So, it could be written better,
    but I hope it's written good enough to understand what does it do :)
6. Written xslt templates, added `downloadImage` & `convertViaXSLT` functions
    - also provided css styles for templates (html) and `copyHTMLFiles` func. that will 
    provide all files are needed 
7. Written `convertToPDF` function
      
## Sources
- [MDN web docs](https://developer.mozilla.org/en-US/docs/Web/API/XMLSerializer)
- [BI-XML lectures](https://moodle-vyuka.cvut.cz/course/view.php?id=2279)
- [XSLT2 tutorials](http://zvon.org/comp/r/tut-XSLT_2.html)
- [w3 schools materials](https://www.w3schools.com/)
- Knizka o XML (lezela v praci, autora si nepamatuji)
